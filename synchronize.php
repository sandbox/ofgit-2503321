<?php
/**
 * @file
 * Copyright © www.it.skyblow.com SkyBlow Company VATIN: PL5170154130.
 *
 * WWW.oFeatures.COM All rights reserved.
 */

include_once 'ofeatureslibrary.php';

$client_id = OfeaturesCustomerServiceLibrary::getOption('ofeatures_customer_service_clientid');

if (isset($client_id)) {
  $base_url             = "https://" . $client_id . ".panel.ofeatures.com";
  $drupal_token             = OfeaturesCustomerServiceLibrary::getOption('ofeatures_customer_service_drupaltoken');
  $ofeatures_features   = OfeaturesCustomerServiceLibrary::getOption('ofeatures_customer_service_features');

  $response = @file_get_contents($base_url . '/skyblow.clientbackend/pluginaccess/features?plugintechnology=drupal'
          . '&websiteaddress='
          . OfeaturesCustomerServiceLibrary::getCurrentDomainNoProtocol()
          . '&token='
          . $drupal_token);

  if ($response != "no-access-rights" && $response !== FALSE) {
    $result         = json_decode($response, TRUE);
    $features_temp  = $result['features'];
    $languages_temp = $result['languages'];

    if (count($features_temp) == 0) {
      echo "no-features-found";
      die();
    }

    echo '<div class="container">';

    foreach ($features_temp as $feature) {
      $codes                           = $feature['codes'];
      $feature_id                      = $feature['id'];
      $ofeatures_features[$feature_id] = array();
      $language_index                  = 0;
      foreach ($languages_temp as $language) {
        $language_index++;
        $code               = $feature['codes'][$language['id']];
        $language_name      = $language['name'];
        $language_id        = $language['id'];
        $feature_name       = $feature['name'];
        $typehumanreadable  = $feature['typehumanreadable'];
        $feature_icon       = $feature['icon'];

        $ofeatures_features[$feature_id][] = array(
          'code' => $code,
          'languageId' => $language_id,
          'languageName' => $language_name,
        );

        $md5_value = md5($feature_id . $language_id);

        echo ("<div class='block-row'><span class='block-info' ><i class='$feature_icon'></i> "
        . "$feature_name ($typehumanreadable) - $language_name language</span>  "
        . "<a href='/admin/structure/block/manage/ofeatures_customer_service/$md5_value/configure' "
        . "class='standard-button'>Add to my website <i class='fa fa-arrow-circle-right'></i></a></div>");
      }
    }
    echo '</div> ';
    OfeaturesCustomerServiceLibrary::setOption('ofeatures_customer_service_features', $ofeatures_features);
  }
  else {
    echo "no-access-rights";
  }
}
else {
  echo "FALSE";
}
die();
