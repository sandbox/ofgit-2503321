/**
 * @file
 * Copyright © www.it.skyblow.com SkyBlow Company VATIN: PL5170154130.
 *
 * WWW.oFeatures.COM All rights reserved.
 */

jQuery(function () {
    var clientBasePanelUrl = "https://" + jQuery('.client-id-property').text() + ".panel.ofeatures.com";
    jQuery('#ofeatures-frame').attr('src', clientBasePanelUrl + "/skyblow.clientbackend/customerservice/alert")
    jQuery('#ofeatures-frame').load(function() {
      jQuery(".preloader-box, .ofeatures-logo").hide()
    })

    jQuery(document).ready(function() {
      if (top.location.href.indexOf("#overlay=") > -1) {
        top.location.href = top.location.href.replace("node#overlay=", "").replace("#overlay=", "")
      }
    })

    function update_support_panel_area() {
      var nav_bar_fixed_height = 0;

      if (jQuery('#toolbar').length > 0) {
        nav_bar_fixed_height = jQuery('#toolbar').height();
      }

      jQuery('#ofeatures-frame').height(jQuery(window).height() - nav_bar_fixed_height - 1)
      jQuery('#ofeatures-frame').css('top', 0)
    }

    setInterval(function() {
      update_support_panel_area()
    }, 300)

    jQuery('.btn-navbar').click(function() {
      setTimeout(update_support_panel_area, 50)
    })

    update_support_panel_area()

    jQuery(window).resize(function() {
      update_support_panel_area()
    })

})
