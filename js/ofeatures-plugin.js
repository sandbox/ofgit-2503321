/**
 * @file
 * Copyright © www.it.skyblow.com SkyBlow Company VATIN: PL5170154130.
 *
 * WWW.oFeatures.COM All rights reserved.
 */

var jQueryOFeatures = jQuery.noConflict();

var invokeAction = function(params, successCallback, failCallback, alwaysCallback, method) {
  if (method == undefined || method == null) {
    method = 'POST';
  }

  jQueryOFeatures.post('/admin/config/ofeatures_customer_service/synchronize', params).done(function(data) {
    if (successCallback != undefined && successCallback != null) {
      successCallback(data)
    }
  }).fail(function(xhr, textStatus, errorThrown) {
    if (failCallback != undefined && failCallback != null) {
      failCallback();
    }
  }).always(function() {
    if (alwaysCallback != undefined && alwaysCallback != null) {
      alwaysCallback()
    }
  });
}

/**
 * Invokes synchronization ajax request (please check synchronize.php).
 */
function synchronize(ofeatures_configuration_no_features_info, currentDomain) {
  invokeAction({plugintype: 'joomla'}, function(data) {
    if (data == "no-access-rights") {
      jQueryOFeatures('.preloader img').fadeOut()
      jQueryOFeatures('.preloader .text').text("Cannot synchronize. Please provide the correct plugin access data.")
      return
    }
    if (data == "no-features-found") {
      jQueryOFeatures('.preloader img').fadeOut()
      var noFeaturesInfo = ofeatures_configuration_no_features_info.replace("[DOMAIN]", currentDomain)
      jQueryOFeatures('.preloader .text').html("<span>" + noFeaturesInfo + "</span>")
      return
    }
    jQueryOFeatures('.preloader').fadeOut()
    jQueryOFeatures('.ok-status').fadeIn()
    jQueryOFeatures('.features').append(data)
    jQueryOFeatures('html,body').animate({scrollTop: jQueryOFeatures('.ok-status').offset().top - 50
    }, 2000);
  }, null, function() {
  })
}

jQueryOFeatures(document).ready(function(){
  synchronize(jQueryOFeatures('.no-features-info').text(), jQueryOFeatures('.current-domain').text());
  setTimeout(function() {
    if (jQueryOFeatures('.block-row').length == 0 && jQueryOFeatures('.no-account-question:visible').length == 0) {
      jQueryOFeatures('.extra-new-account').fadeIn()
    }
  }, 5000)
})
