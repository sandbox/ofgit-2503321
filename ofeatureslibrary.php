<?php
/**
 * @file
 * Copyright © www.it.skyblow.com SkyBlow Company VATIN: PL5170154130.
 *
 * WWW.oFeatures.COM All rights reserved.
 */

/**
 * Ofeatures Customer Service Library.
 */
abstract class OfeaturesCustomerServiceLibrary {

  /**
   * Gets option by name.
   */
  public static function getOption($option_name) {
    return variable_get($option_name, '');
  }

  /**
   * Sets option by name.
   */
  public static function setOption($option_name, $value) {
    return variable_set($option_name, $value);
  }

  /**
   * Gets domain.
   */
  public static function getCurrentDomainNoProtocol() {
    return str_replace('www.', '', $_SERVER['SERVER_NAME']);
  }

}
