<?php
/**
 * @file
 * Copyright © www.it.skyblow.com SkyBlow Company VATIN: PL5170154130.
 *
 * WWW.oFeatures.COM All rights reserved.
 */


include_once 'ofeatureslibrary.php';

$client_id = OfeaturesCustomerServiceLibrary::getOption('ofeatures_customer_service_clientid');
$client_id = trim($client_id);

$preloader_path = "/" . OFEATURES_CUSTOMER_SERVICE_PATH . '/img/preloader.gif';
$logo_path = "/" . OFEATURES_CUSTOMER_SERVICE_PATH . '/img/ofeatures-logo-128px128px.png';

?>

<?php if (!empty($client_id)): ?>
  <data>
    <div class="client-id-property"><?php echo $client_id; ?></div>
  </data>
  <img alt="" class="ofeatures-logo" src="<?php echo $logo_path; ?>" /><br/>
  <br/>
  <h4 class="preloader-box">
    <?php echo t("Loading Support Panel...") ?><br/>
    <img alt="" src="<?php echo $preloader_path; ?>" />
  </h4>
  <div>
  <iframe  id="ofeatures-frame"></iframe>
<?php else: ?>
  <h2 class="access-data-info">
    <img alt="" src="<?php echo $logo_path; ?>" />
    <br/><br/>
    Please provide the correct plugin access data in: <a href='/admin/config/ofeatures_customer_service'>Modules > oFeatures Customer Service > Configure</a>
  </h2>
<?php endif; ?>
