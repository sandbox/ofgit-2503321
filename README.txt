CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Requirements
 * Configuration

INTRODUCTION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * You may want to disable Toolbar module, since its output clashes with
   Administration Menu.

The secret to success in a business are satisfied customers. 
With our tools integrated into one platform you will be able 
to manage your Customer Service easily and efficiently. 
With this plugin you can login to the Support Panel directly 
from the WordPress admin panel. 
It also lets you publish features on your website in a more 
convenient way.

You can contact us at any time to get support, leave feedback, 
request feature or functionality:

- Via Skype: ofeatures.support
- On our website: www.ofeatures.com


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


REQUIREMENTS
------------
No special requirements
 

CONFIGURATION
-------------

 * Configure oFeatures access data in 
 Administration » Modules » oFeatures Customer Service Configuration
