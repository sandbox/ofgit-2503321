<?php
/**
 * @file
 * Copyright © www.it.skyblow.com SkyBlow Company VATIN: PL5170154130.
 *
 * WWW.oFeatures.COM All rights reserved.
 */

include_once 'ofeatureslibrary.php';

$current_domain                           = OfeaturesCustomerServiceLibrary::getCurrentDomainNoProtocol();
$ofeatures_configuration_features_title   = t("Drupal blocks that you can use:");
$ofeatures_configuration_no_features_info = t("You don't have any Features added to website: '@domain' on your oFeatures account.<br/>Please check that you have entered the correct website address by going to your oFeatures account > Websites > Settings", array('@domain' => $current_domain));
$ofeatures_features                       = OfeaturesCustomerServiceLibrary::getOption('ofeatures_customer_service_features');

$preloader_path = "/" . OFEATURES_CUSTOMER_SERVICE_PATH . "/img/preloader.gif";

?>


<data>
  <property class="no-features-info"><?php echo $ofeatures_configuration_no_features_info; ?></property>
  <property class="current-domain"><?php echo OfeaturesCustomerServiceLibrary::getCurrentDomainNoProtocol(); ?></property>
</data>
<i class="icons-cache fa fa-thumbs-o-up"></i>
<form  autocomplete="off"  action="index.php?option=com_ofeatures_customer_service&view=configuration" method="post" id="ofeaturesAccess" name="ofeaturesAccess">
  <h4 class="ok-status">
    <span class="text">
      <?php echo t("All features have been synchronized") ?> <i class="fa fa-check-circle"></i>
      <span class="synchronize-again-btn" onclick="location.reload()">synchronize again</span><br/>
      <?php echo t("Every time you change something in your features (for example, the style) please visit this configuration page again to keep your features updated.") ?> <br/>
      <br/><br/>
      <h3><?php echo $ofeatures_configuration_features_title; ?></h3>
    </span>
  </h4>
  <div class="features"></div>
  <?php if (OfeaturesCustomerServiceLibrary::getOption('ofeatures_customer_service_clientid')): ?>
    <h4 class="preloader"><span class="text"><?php echo t("Synchronizing your features...") ?></span><br/>
      <img alt="" src="<?php echo $preloader_path ?>" />
    </h4>
  <?php endif; ?>
  <br/>
  <h3 class="details-request extra-new-account">
    <span class="no-account-question"><?php echo t("Don't Have an Account?") ?><a class="standard-button orange new-account-btn" target="_blank" href="http://ofeatures.com?utm_source=cms&utm_medium=button-bottom&utm_campaign=drupal-plugin">Create oFeatures account <i class="fa fa-arrow-circle-right"></i></a></span>
  </h3>
  <br/><br/><br/><br/><br/>
</form>
